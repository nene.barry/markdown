# Presentation Title

## Month XYth, 2019

<div style="top: 6em; left: 0%; position: absolute;">
    <img src="theme/img/lcsb_bg.png">
</div>

<div style="top: 5em; left: 60%; position: absolute;">
    <img src="slides/img/r3-training-logo.png" height="200px">
    <br><br><br>
    <h1>A catchy title</h1>
    <br><br><br>
    <h4>
        Firstname LastName, Title<br><br>
        firstname.lastname@uni.lu<br><br>
        <i>Luxembourg Centre for Systems Biomedicine</i>
    </h4>
</div>
