# Additional syntax and formatting

Special syntax (through HTML comments) is available for adding attributes to Markdown elements.

## Slide transition
**data-transition** attribute is used to change default slide transition.
  * possible values of *data-transition* attribute: zoom, fade, convex, convex, slide, strike, linear  + "-in" and "-out" optional suffixes
  * possible values of *data-transition-speed*: fast, slow
  * examples:
    * Markdown
      * fade-out
  ```
  # My fade-in fade-out slide title
  <!-- .slide: data-transition="fade" -->
  ```
      * zoom-in fade-out
  ```
  # My zoom-in fade-out slide title
  <!-- .slide: data-transition="zoom-in fade-out" data-transition-speed="fast"-->
  ```
    * HTML
      * fade-in fade-out
  ```
  <section data-transition="fade">
  # My fade-in fade-out slide title
  </section>
  ```

## New slide
  * Markdown - three empty lines or separate .md file
  * HTML syntax - \<section\> element

## Fragmentation
It is possible to adjust behavior of fragments of your slide, e.g. order in which the elements are shown can be defined by class *fragment* with optional attribute data-fragment-index:
  * Markdown
  ```
    * first element  <!-- .element: class="fragment"-->
  <!-- .element: class="fragment" data-fragment-index="3" -->
    * third element
  <!-- .element: class="fragment" data-fragment-index="2" -->
    * second element
  ```
  * HTML syntax
  ```
  ...
  <div class="fragment" data-fragment-index="3">
    third element
  </div>
  ...
  ```
  additional classes extending class fragment:
    * special fragment entry: fade-in-then-semi-out, fade-in-then-out, semi-fade-out, zoom-in, fade-up, fade-left, ...
    * highlighting: highlight-green, highlight-blue, highlight-red, highlight-current-red, ...
    * hiding fragments: current-visible
    * simple animation features: grow, shrink, strike

# External resources

For more tips on supported functionality visit official [reveal.js GitHub repository](https://github.com/hakimel/reveal.js/).