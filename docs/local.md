
# Local method

## Prerequisites

In order to use the slide deck, you must have:

- **git**: For convenience, have your SSH key configured on git-r3lab. [Here](https://docs.gitlab.com/ee/gitlab-basics/create-your-ssh-keys.html) is how to do it.
- **git-lfs**: Installation instructions are [here](https://help.github.com/en/articles/installing-git-large-file-storage)
- **Python 3**: It is recommend to use a virtual environment. Information on how to create a virtual environment is [here](https://virtualenvwrapper.readthedocs.io/en/latest/).
- **yarn**: Instructions are [here](https://yarnpkg.com/lang/en/docs/install)

## How to get started

- Fork this repository
- Clone your fork
    ```bash
    $ git clone --recurse-submodules ssh://git@git-r3lab-server.uni.lu:8022/<yourUsername>/presentations-internal.git
    ```
- If the `--recurse-submodules` flag has been omitted, run this command to check for the missing modules and update them:
    ```bash
    $ git submodule update --init --recursive
    ```
- Open a terminal and work on your presentation:
    ```bash
    $ cd presentations-internal
    $ pip install .
    $ contribute --date="YYYY-MM-DD" --name="yourAwesomeTitle"
    ```

## <img src="https://webdav-r3lab.uni.lu/public/R3lab/logos/warning.png" height="20px" width="20px"> Tips

- Please create a branch from `develop` before starting to work on your presentation. You could name it `YYYY-MM-DD_yourAwesomeTitle`.
- If you are done with making changes to your presentation, push them to your fork and open a merge request against the `develop` branch.

## <img src="https://webdav-r3lab.uni.lu/public/R3lab/logos/warning.png" height="20px" width="20px">Solving the error: `Fatal error: Unable to find local grunt.`

If you get a fatal error complaining about your local grunt installation, please make sure that you have add the `--recurse-submodules` flag when cloning, or that you ran the `git submodule update --init --recursive` command from within
the `presentations-internal` directory.

Please also make sure that you run the `contribute` command from the root of the `presentations-internal` directory.